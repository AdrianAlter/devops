## Install Prometheus and Enable Monitoring on Lens (in manifests/elastic folder)
kubectl create namespace monitoring
kubectl create configmap prometheus-config --from-file=prometheus.yaml -n monitoring
kubectl apply -f prometheusDeployment.yaml
kubectl apply -f prometheusService.yaml


```bash
https://github.com/vitobotta/hetzner-k3s
```
Then execute the instructions and get kubernetes ready.

Next we need to install nginx controller to enable use of LoadBalancers

```bash
helm repo add nginx-stable https://helm.nginx.com/stable
helm repo update
helm install nginx-ingress nginx-stable/nginx-ingress
```
Go ahead now and create a LoadBalancer in hetzner cloud, on the same cloud project.
Now we create a Service to link it up:

```yaml
apiVersion: v1
kind: Service
metadata:
  name: example-service
  annotations:
    load-balancer.hetzner.cloud/location: hel1
spec:
  selector:
    app: example
  ports:
    - port: 80
      targetPort: 8080
  type: LoadBalancer
```

## Installing on VPS
git clone https://gitlab.com/AdrianAlter/devops.git
cd devops
cd hetzner
wget https://github.com/vitobotta/hetzner-k3s/releases/download/v1.1.5/hetzner-k3s-linux-amd64
chmod +x hetzner-k3s-linux-amd64
sudo mv hetzner-k3s-linux-amd64 /usr/local/bin/hetzner-k3s
hetzner-k3s create --config cluster_config.yaml
hetzner-k3s create --config cluster_config_new.yaml

## Delete Cluster
hetzner-k3s delete --config cluster_config.yaml

## Install Kubectl VPS
curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
sudo install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl

## Install monitoring
kubectl apply -f https://github.com/kubernetes-sigs/metrics-server/releases/latest/download/components.yaml

## License

[MIT](https://choosealicense.com/licenses/mit/)
